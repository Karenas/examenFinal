﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductsApp.Controllers;

namespace ProductsApp.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var controller = new ProductsController();
            var res = controller.GetAllProducts();
            Assert.IsNotNull(res);
        }
    }
}
